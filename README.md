# binary blobs for hammerhead

All blobs comes from:

https://developers.google.com/android/images

* hammerhead-m4b30x-factory-10cfaa5c.zip
* hammerhead-m4b30z-factory-625c027b.zip
* bullhead-n4f26i-factory-03896e0a.zip
* angler-opr6.170623.019-factory-9fd72ad6.zip

https://wiki.lineageos.org/devices/oneplus3/

* lineage-18.1-20220822-nightly-oneplus3-signed.zip

https://wiki.lineageos.org/devices/d802/

* lineage-18.1-20220825-nightly-d802-signed.zip

https://wiki.lineageos.org/devices/bacon/

* lineage-18.1-20220825-nightly-bacon-signed.zip

https://wiki.lineageos.org/devices/klte/

* lineage-18.1-20220821-nightly-klte-signed

You can check it by running:

* https://gitlab.com/axet/homebin/-/blob/debian/homebin.d/lineageos/firmware_blob_check.sh

Original repository, does not explain some issues:

1) Unknown source of, looks like those comming from old nexus5 cm14 build:

* ./proprietary/vendor/lib/soundfx/libqcbassboost.so
* ./proprietary/vendor/lib/soundfx/libqcreverb.so
* ./proprietary/vendor/lib/soundfx/libqcvirt.so

## Output

```
./proprietary/vendor/bin/vss_init - hammerhead-lrx22c
./proprietary/vendor/bin/bridgemgrd - hammerhead-m4b30x
./proprietary/vendor/bin/diag_klog - hammerhead-m4b30x
./proprietary/vendor/bin/diag_mdlog - hammerhead-m4b30x
./proprietary/vendor/bin/ds_fmc_appd - hammerhead-m4b30x
./proprietary/vendor/bin/irsc_util - hammerhead-m4b30x
./proprietary/vendor/bin/mm-qcamera-daemon - hammerhead-m4b30x
./proprietary/vendor/bin/netmgrd - hammerhead-m4b30x
./proprietary/vendor/bin/nl_listener - hammerhead-m4b30x
./proprietary/vendor/bin/port-bridge - hammerhead-m4b30x
./proprietary/vendor/bin/qmuxd - hammerhead-m4b30x
./proprietary/vendor/bin/qseecomd - hammerhead-m4b30x
./proprietary/vendor/bin/radish - hammerhead-m4b30x
./proprietary/vendor/bin/rmt_storage - hammerhead-m4b30x
./proprietary/vendor/bin/subsystem_ramdump - hammerhead-m4b30x
./proprietary/vendor/bin/usbhub - hammerhead-m4b30x
./proprietary/vendor/bin/usbhub_init - hammerhead-m4b30x
./proprietary/vendor/bin/mpdecision - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/bin/sensors.qcom - hammerhead-m4b30x
./proprietary/vendor/firmware/discretix/dxhdcp2.b00 - hammerhead-lrx22c
./proprietary/vendor/firmware/discretix/dxhdcp2.b01 - hammerhead-lrx22c
./proprietary/vendor/firmware/discretix/dxhdcp2.b02 - hammerhead-lrx22c
./proprietary/vendor/firmware/discretix/dxhdcp2.b03 - hammerhead-lrx22c
./proprietary/vendor/firmware/discretix/dxhdcp2.mdt - hammerhead-lrx22c
./proprietary/vendor/firmware/keymaster/keymaster.b00 - hammerhead-lrx22c
./proprietary/vendor/firmware/keymaster/keymaster.b01 - hammerhead-lrx22c
./proprietary/vendor/firmware/keymaster/keymaster.b02 - hammerhead-lrx22c
./proprietary/vendor/firmware/keymaster/keymaster.b03 - hammerhead-lrx22c
./proprietary/vendor/firmware/keymaster/keymaster.mdt - hammerhead-lrx22c
./proprietary/vendor/firmware/a330_pfp.fw - hammerhead-lrx22c
./proprietary/vendor/firmware/a330_pm4.fw - hammerhead-lrx22c
./proprietary/vendor/firmware/adsp.b00 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b01 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b02 - hammerhead-lrx22c
./proprietary/vendor/firmware/adsp.b03 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b04 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b05 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b06 - hammerhead-lrx22c
./proprietary/vendor/firmware/adsp.b07 - hammerhead-lrx22c
./proprietary/vendor/firmware/adsp.b08 - hammerhead-lrx22c
./proprietary/vendor/firmware/adsp.b09 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b10 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b11 - hammerhead-m4b30x
./proprietary/vendor/firmware/adsp.b12 - hammerhead-lrx22c
./proprietary/vendor/firmware/adsp.mdt - hammerhead-m4b30x
./proprietary/vendor/firmware/bcm2079x-b5_firmware.ncd - hammerhead-lrx22c
./proprietary/vendor/firmware/bcm2079x-b5_pre_firmware.ncd - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_2_DATA1.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_2_DATA2.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_2_DATA3.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_3_CAL.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_3_DATA1.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_3_DATA2.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/bu24205_LGIT_VER_3_DATA3.bin - hammerhead-lrx22c
./proprietary/vendor/firmware/cmnlib.b00 - hammerhead-lrx22c
./proprietary/vendor/firmware/cmnlib.b01 - hammerhead-lrx22c
./proprietary/vendor/firmware/cmnlib.b02 - hammerhead-lrx22c
./proprietary/vendor/firmware/cmnlib.b03 - hammerhead-lrx22c
./proprietary/vendor/firmware/cmnlib.mdt - hammerhead-lrx22c
./proprietary/vendor/firmware/venus.b00 - hammerhead-m4b30x
./proprietary/vendor/firmware/venus.b01 - hammerhead-m4b30x
./proprietary/vendor/firmware/venus.b02 - hammerhead-m4b30x
./proprietary/vendor/firmware/venus.b03 - hammerhead-m4b30x
./proprietary/vendor/firmware/venus.b04 - hammerhead-lrx22c
./proprietary/vendor/firmware/venus.mdt - hammerhead-m4b30x
./proprietary/vendor/firmware/widevine.b00 - bullhead-n4f26i
./proprietary/vendor/firmware/widevine.b01 - bullhead-n4f26i
./proprietary/vendor/firmware/widevine.b02 - bullhead-n4f26i
./proprietary/vendor/firmware/widevine.b03 - bullhead-n4f26i
./proprietary/vendor/firmware/widevine.mdt - bullhead-n4f26i
./proprietary/vendor/firmware/cpp_firmware_v1_1_1.fw - hammerhead-lrx22c
./proprietary/vendor/firmware/cpp_firmware_v1_1_6.fw - hammerhead-lrx22c
./proprietary/vendor/firmware/cpp_firmware_v1_2_0.fw - hammerhead-lrx22c
./proprietary/vendor/lib/egl/libplayback_adreno.so - hammerhead-m4b30x
./proprietary/vendor/lib/egl/eglsubAndroid.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/egl/libEGL_adreno.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/egl/libGLESv1_CM_adreno.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/egl/libGLESv2_adreno.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/egl/libq3dtools_adreno.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/hw/flp.default.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/hw/sensors.msm8974.so - hammerhead-m4b30x
./proprietary/vendor/lib/mediadrm/libwvdrmengine.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libAKM8963.so - hammerhead-m4b30x
./proprietary/vendor/lib/libCommandSvc.so - hammerhead-m4b30x
./proprietary/vendor/lib/libFuzzmmstillomxenc.so - hammerhead-m4b30x
./proprietary/vendor/lib/libacdbrtac.so - hammerhead-m4b30x
./proprietary/vendor/lib/libadiertac.so - hammerhead-m4b30x
./proprietary/vendor/lib/libaudcal.so - hammerhead-m4b30x
./proprietary/vendor/lib/libconfigdb.so - hammerhead-m4b30x
./proprietary/vendor/lib/libdiag.so - hammerhead-m4b30x
./proprietary/vendor/lib/libdsi_netctrl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libdsutils.so - hammerhead-m4b30x
./proprietary/vendor/lib/libfrsdk.so - bullhead-n4f26i
./proprietary/vendor/lib/libidl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libjpegdhw.so - hammerhead-m4b30x
./proprietary/vendor/lib/libjpegehw.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_c2d_module.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_cpp_module.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_iface_modules.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_imglib_modules.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_isp_modules.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_pproc_modules.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_sensor_modules.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_stats_algorithm.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_stats_modules.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_vpe_module.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera2_wnr_module.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_faceproc.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_imglib.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_imx179_eeprom.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmipl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmjpeg.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmqjpeg_codec.so - hammerhead-m4b30x
./proprietary/vendor/lib/libnetmgr.so - hammerhead-m4b30x
./proprietary/vendor/lib/liboemcamera.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqcci_legacy.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqdi.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqdp.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqmi.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqmi_client_qmux.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqmi_common_so.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqmi_encdec.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqmiservices.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqomx_jpegenc.so - hammerhead-m4b30x
./proprietary/vendor/lib/libril-qcril-hook-oem.so - hammerhead-m4b30x
./proprietary/vendor/lib/libsensor1.so - hammerhead-m4b30x
./proprietary/vendor/lib/libsensor_reg.so - hammerhead-m4b30x
./proprietary/vendor/lib/libsensor_user_cal.so - hammerhead-m4b30x
./proprietary/vendor/lib/libthermalclient.so - hammerhead-m4b30x
./proprietary/vendor/lib/libthermalioctl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libtime_genoff.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvdmengine.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvdmfumo.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvss_common_core.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvss_common_idl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvss_common_iface.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvss_nv_core.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvss_nv_idl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libvss_nv_iface.so - hammerhead-m4b30x
./proprietary/vendor/lib/libI420colorconvert.so - hammerhead-m4b30x
./proprietary/vendor/lib/libQSEEComAPI.so - hammerhead-m4b30x
./proprietary/vendor/lib/libadsprpc.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_imx179_common.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_imx179_default_video.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_imx179_preview.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_imx179_snapshot.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_mt9m114b_common.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_mt9m114b_default_video.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_mt9m114b_preview.so - hammerhead-m4b30x
./proprietary/vendor/lib/libchromatix_mt9m114b_snapshot.so - hammerhead-m4b30x
./proprietary/vendor/lib/libdrmfs.so - hammerhead-m4b30x
./proprietary/vendor/lib/libdrmtime.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmm-abl.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmm-color-convertor.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmQSM.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_hdr_lib.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_image_stab.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_imx179.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_mt9m114b.so - hammerhead-m4b30x
./proprietary/vendor/lib/libmmcamera_wavelet_lib.so - hammerhead-m4b30x
./proprietary/vendor/lib/liboemcrypto.so - hammerhead-m4b30x
./proprietary/vendor/lib/libril-qc-qmi-1.so - hammerhead-m4b30x
./proprietary/vendor/lib/librpmb.so - hammerhead-m4b30x
./proprietary/vendor/lib/libssd.so - hammerhead-m4b30x
./proprietary/vendor/lib/libstagefright_hdcp.so - hammerhead-m4b30x
./proprietary/vendor/lib/libxml.so - hammerhead-m4b30x
./proprietary/vendor/lib/libC2D2.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libCB.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libDxHdcp.so - hammerhead-m4b30x
./proprietary/vendor/lib/libOmxAacDec.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libOmxAmrwbplusDec.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libOmxEvrcDec.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libOmxQcelp13Dec.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libOmxWmaDec.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libOpenCL.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libacdbloader.so - hammerhead-m4b30x
./proprietary/vendor/lib/libadreno_utils.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libc2d30-a3xx.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libflp.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libgeofence.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libgsl.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libizat_core.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/liblbs_core.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libllvm-qcom.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libloc_api_v02.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libmdmdetect.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libperipheral_client.so - lineage-18.1-20220825-nightly-bacon-signed
./proprietary/vendor/lib/libprotobuf-cpp-lite-v29.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libqmi_cci.so - hammerhead-m4b30x
./proprietary/vendor/lib/libqmi_csi.so - hammerhead-m4b30x
./proprietary/vendor/lib/libsc-a3xx.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libscale.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libuiblur.so - lineage-18.1-20220825-nightly-d802-signed
./proprietary/vendor/lib/libloc_ds_api.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libqti-perfd-client.so - lineage-18.1-20220821-nightly-klte-signed
./proprietary/vendor/lib/libdrmdiag.so - hammerhead-m4b30x
./proprietary/vendor/etc/acdbdata/MTP/MTP_Bluetooth_cal.acdb - hammerhead-lrx22c
./proprietary/vendor/etc/acdbdata/MTP/MTP_General_cal.acdb - hammerhead-lrx22c
./proprietary/vendor/etc/acdbdata/MTP/MTP_Global_cal.acdb - hammerhead-lrx22c
./proprietary/vendor/etc/acdbdata/MTP/MTP_Handset_cal.acdb - hammerhead-m4b30x
./proprietary/vendor/etc/acdbdata/MTP/MTP_Hdmi_cal.acdb - hammerhead-lrx22c
./proprietary/vendor/etc/acdbdata/MTP/MTP_Headset_cal.acdb - hammerhead-m4b30x
./proprietary/vendor/etc/acdbdata/MTP/MTP_Speaker_cal.acdb - hammerhead-m4b30x
./proprietary/vendor/etc/DxHDCP.cfg - hammerhead-lrx22c
./proprietary/vendor/etc/qcril.db - hammerhead-m4b30x
./proprietary/vendor/etc/sensor_def_hh.conf - hammerhead-lrx22c
./proprietary/vendor/framework/serviceitems.jar - hammerhead-m4b30x
./proprietary/vendor/priv-app/OmaDmclient/OmaDmclient.apk - hammerhead-m4b30x
./proprietary/vendor/priv-app/SprintHiddenMenu/SprintHiddenMenu.apk - hammerhead-m4b30x

./proprietary/vendor/app/UpdateSetting/UpdateSetting.apk - hammerhead-m4b30x
./proprietary/vendor/app/qcrilmsgtunnel/qcrilmsgtunnel.apk - hammerhead-m4b30x 
./proprietary/vendor/app/shutdownlistener/shutdownlistener.apk - hammerhead-m4b30x
./proprietary/vendor/framework/qcrilhook.jar - hammerhead-m4b30x

./proprietary/vendor/lib/soundfx/libqcbassboost.so - unknown source deivce/firmware, 932f1fe f97871c08285a86b29f2ac8b514b8cda
./proprietary/vendor/lib/soundfx/libqcreverb.so - unknown source deivce/firmware, 932f1fe 0b6a5e2738a92684f7569973e287a5d3
./proprietary/vendor/lib/soundfx/libqcvirt.so - unknown source deivce/firmware, 932f1fe 3e2fe20968211610ee3dbdc9527de3b9
```

## Links:

* https://github.com/z3DD3r/android_vendor_lge
